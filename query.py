#!/bin/python
import pprint
from elasticsearch import Elasticsearch
from datetime import datetime

es = Elasticsearch(hosts=['10.10.0.225'])

query = {"query":{"bool":{"must":{},"must_not":{},"should":{},"filter":{}}}}

result = es.search(index='python_index',body=query)

# For pretty printing the JSON
pp = pprint.PrettyPrinter()
pp.pprint(result)


