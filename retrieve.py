#!/bin/python
import pprint
from elasticsearch import Elasticsearch
from datetime import datetime

es = Elasticsearch(hosts=['10.10.0.225'])

result = es.get(index='python_index',doc_type='_doc',id=1,_source=True)

# For pretty printing the JSON
pp = pprint.PrettyPrinter()
pp.pprint(result)


